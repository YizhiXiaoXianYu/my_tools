module tools

go 1.20

require (
	github.com/pkg/sftp v1.13.6
	github.com/sirupsen/logrus v1.9.3
	golang.org/x/crypto v0.22.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/kr/fs v0.1.0 // indirect
	golang.org/x/sys v0.19.0 // indirect
)
