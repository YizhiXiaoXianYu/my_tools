package tools

import (
	"os"

	"gopkg.in/yaml.v2"
)

// F_YAML工具_读配置文件 用于读取并解析指定路径下的YAML配置文件，将解析结果存储到传入的对象中。
// 参数：
// 配置文件路径 string - 配置文件的路径。
// 对象 interface{} - 用于存储解析结果的对象指针。
// 返回值：
// error - 如果读取或解析过程中发生错误，返回相应的错误信息；否则返回nil。
func F_YAML工具_读配置文件(配置文件路径 string, 对象 interface{}) error {
	// 读取配置文件内容
	yamlfile, err := os.ReadFile(配置文件路径)
	if err != nil {
		// 如果读取文件时发生错误，直接返回错误
		return err
	}
	// 解析YAML文件内容到对象
	yaml.Unmarshal(yamlfile, 对象)
	return nil
}
