package tools

import (
	"strings"
	"time"
)

func F_获取当前时间() string {
	// 获取当前时间
	currentTime := time.Now()
	// 获取当前日期
	currentDate := currentTime.Format("2006-01-02 15:04:05")
	return currentDate
}
func F_获取适合文件名的时间() string {
	temp := F_获取当前时间()
	return strings.Replace(strings.Replace(temp, " ", "-", -1), ":", "-", -1)
}
