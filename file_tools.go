package tools

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"reflect"
	"strings"
)

func F_文件操作_按行读文件(文件路径 string, 回调 func(string)) {
	// 打开文件
	file, err := os.Open(文件路径)
	if err != nil {
		fmt.Println("Error opening file:", err)
	}
	defer file.Close()
	// 创建一个 Scanner 以按行读取文件
	scanner := bufio.NewScanner(file)
	// 逐行读取文件内容
	for scanner.Scan() {
		回调(scanner.Text())
	}
	// 检查是否有读取错误
	if err := scanner.Err(); err != nil {
		fmt.Println("Error reading file:", err)
	}
}

func F_文件操作_读取文本并转换为json结构体(文件路径 string, 结构体指针 interface{}) {
	if !isPointer(结构体指针) {
		fmt.Println("参数二非指针类型.")
		return
	}
	file, err := os.Open(文件路径)
	if err != nil {
		fmt.Println("打开文件时异常:", err)
		return
	}
	defer file.Close()
	数据集, err := io.ReadAll(file)
	if err != nil {
		fmt.Println("读文件时异常:", err)
		return
	}

	err = json.Unmarshal(数据集, 结构体指针)
	if err != nil {
		fmt.Println("转换结构体时异常e:", err)
		return
	}

}
func isPointer(i interface{}) bool {
	return reflect.ValueOf(i).Kind() == reflect.Ptr
}
func F_文件操作_修改文件中的某行数据(文件路径, 待修改的文本内容, 修改的文本内容 string, 是否跳过注释 bool) {
	filePath := 文件路径
	file, err := os.OpenFile(filePath, os.O_RDWR, 0644)
	if err != nil {
		fmt.Println("Failed to open file:", err)
		return
	}
	defer file.Close()

	// 创建一个 scanner 以逐行读取文件
	scanner := bufio.NewScanner(file)
	var lines []string
	for scanner.Scan() {
		line := scanner.Text()

		// 在这里修改想要修改的行
		if strings.Contains(line, 待修改的文本内容) {
			if 是否跳过注释 {
				if strings.HasPrefix(line, "#") {
					lines = append(lines, line)
					continue
				}
			}
			// 修改行数据
			line = 修改的文本内容
		}
		lines = append(lines, line)
	}

	// 将修改后的内容写回到文件中
	file.Truncate(0)                // 清空文件内容
	file.Seek(0, 0)                 // 定位到文件开头
	writer := bufio.NewWriter(file) // 创建一个新的写入器
	for _, line := range lines {
		_, err := writer.WriteString(line + "\n")
		if err != nil {
			fmt.Println("写出文件失败:", err)
			return
		}
	}
	writer.Flush() // 刷新缓冲区，确保数据写入文件

	fmt.Println("文件修改成功!")
}
