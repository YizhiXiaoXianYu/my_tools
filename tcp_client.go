package tools

import (
	"fmt"
	"net"

	log "github.com/sirupsen/logrus"
)

var Host, Username, Password, types string
var number int
var id string
var sleepTime int64

type Tcp客户端 struct {
	conn *net.Conn
}

func (t *Tcp客户端) F_连接(host string, port int) {
	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%d",host,port))
	if err != nil {
		log.Errorf("连接tcp协议失败 ,err:%s\n", err)
		return
	}
	t.conn = &conn
	// defer conn.Close()
	// for {
	// 	fmt.Println(index)
	// 	satrt := time.Now().Unix()
	// 	randomNum := 1024
	// 	// 响应服务端信息
	// 	_, err = conn.Write([]byte(strconv.Itoa(randomNum)))
	// 	// time.Sleep(30 * time.Second)
	// 	if err != nil {
	// 		fmt.Println("Write failed,err:", err)
	// 		connect(id, ip)
	// 		break
	// 	}
	// 	size := 1024
	// 	temp := make([]byte, size)
	// 	// 响应服务端信息
	// 	// sum := 0
	// 	// for size > sum {
	// 	_, err := conn.Read(temp)
	// 	if err != nil {
	// 		fmt.Println("Read failed,err:", err)
	// 		connect(id, ip)
	// 		return
	// 	}
	// 	sum += i
	// fmt.Printf("传输数据：%d\n", sum)
	// }

}

func (t *Tcp客户端) F_发送数据(数据 string) string {
	conn := *t.conn
	_, err := conn.Write(make([]byte, 100))
	if err != nil {
		log.Errorf("写出数据时错误:%s\n", err)
		return ""
	}
	temp := make([]byte, 1024)
	n, err := conn.Read(temp)
	if err != nil {
		log.Errorf("读取数据时错误:%s\n", err)
		return ""
	}
	return string(temp[:n])
}
